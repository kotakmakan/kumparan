package storage

import (
	"gitlab.com/kotakmakan/kumparan/types"
)

// AuthorStorage Holds Authors temporarily
type AuthorStorage struct {
	Authors []types.Author
}

// CreateAuthorStorage adds temporary in-memory storage for Authors
func CreateAuthorStorage() *AuthorStorage {
	return &AuthorStorage{
		Authors: []types.Author{
			types.Author{
				ID:        1,
				Firstname: "Bruce",
				Lastname:  "Wayne",
			},
			types.Author{
				ID:        2,
				Firstname: "Peter",
				Lastname:  "Parker",
			},
		},
	}
}
