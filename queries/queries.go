package queries

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/kotakmakan/kumparan/storage"
)

// QueryAuthor initialize struct for storing author
type QueryAuthor struct {
	Storage *storage.AuthorStorage
}

// NewQueryAuthor returns a new in-memory data store for author
func NewQueryAuthor(storage *storage.AuthorStorage) *QueryAuthor {
	return &QueryAuthor{Storage: storage}
}

// GetRootFields returns all the available queries.
func (q *QueryAuthor) GetRootFields() graphql.Fields {
	return graphql.Fields{
		"authors": q.GetAllAuthorsQuery(),
		"author":  q.GetAuthorQuery(),
	}
}
