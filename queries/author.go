package queries

import (
	"log"

	"github.com/graphql-go/graphql"
	"gitlab.com/kotakmakan/kumparan/types"
)

// GetAllAuthorsQuery returns all available Authors
func (q *QueryAuthor) GetAllAuthorsQuery() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.AuthorType),
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			storage := q.Storage
			return storage.Authors, nil
		},
	}
}

// GetAuthorQuery returns single Author
func (q *QueryAuthor) GetAuthorQuery() *graphql.Field {
	return &graphql.Field{
		Type: types.AuthorType,
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			id, ok := params.Args["id"].(int)
			if ok {
				storage := q.Storage
				log.Print(storage.Authors)
				for _, author := range storage.Authors {
					if int(author.ID) == id {
						return author, nil
					}
				}
			}
			return nil, nil
		},
	}
}
