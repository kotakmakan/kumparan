package mutations

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/kotakmakan/kumparan/storage"
)

type MutationServer struct {
	Storage *storage.AuthorStorage
}

func NewMutationServer(storage *storage.AuthorStorage) *MutationServer {
	return &MutationServer{Storage: storage}
}

// GetRootFields returns all the available mutations.
func (m *MutationServer) GetRootFields() graphql.Fields {
	return graphql.Fields{
		"createAuthor": m.GetCreateAuthorMutation(),
	}
}
