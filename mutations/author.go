package mutations

import (
	"log"

	"github.com/graphql-go/graphql"
	"gitlab.com/kotakmakan/kumparan/types"
)

// GetCreateAuthorMutation creates a new author and returns it
func (m *MutationServer) GetCreateAuthorMutation() *graphql.Field {
	return &graphql.Field{
		Type: types.AuthorType,
		Args: graphql.FieldConfigArgument{
			"firstname": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"lastname": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			author := &types.Author{
				Firstname: params.Args["firstname"].(string),
				Lastname:  params.Args["lastname"].(string),
			}

			storage := m.Storage

			storage.Authors = append(storage.Authors, *author)

			log.Print(*author)
			return author, nil
		},
	}
}
