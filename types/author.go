package types

import (
	"github.com/graphql-go/graphql"
)

// Author type definition.
type Author struct {
	ID        int    `db:"id" json:"id"`
	Firstname string `db:"firstname" json:"firstname"`
	Lastname  string `db:"lastname" json:"lastname"`
}

// AuthorType is the GraphQL schema for the user type.
var AuthorType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Author",
	Fields: graphql.Fields{
		"id":        &graphql.Field{Type: graphql.Int},
		"firstname": &graphql.Field{Type: graphql.String},
		"lastname":  &graphql.Field{Type: graphql.String},
		"articles": &graphql.Field{
			Type: graphql.NewList(ArticleType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				var articles []Article

				// Implement logic to retrieve user associated article from user id here.

				return articles, nil
			},
		},
	},
})
