package types

import (
	"github.com/graphql-go/graphql"
)

// Article type definition.
type Article struct {
	ID    int    `db:"id" json:"id"`
	Title string `db:"title" json:"title"`
}

// ArticleType is the GraphQL schema for the user type.
var ArticleType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Article",
	Fields: graphql.Fields{
		"id":    &graphql.Field{Type: graphql.Int},
		"title": &graphql.Field{Type: graphql.String},
	},
})
