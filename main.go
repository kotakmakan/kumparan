package main

import (
	"log"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"github.com/labstack/echo"
	"gitlab.com/kotakmakan/kumparan/mutations"
	"gitlab.com/kotakmakan/kumparan/queries"
	"gitlab.com/kotakmakan/kumparan/storage"
)

func main() {

	storage := storage.CreateAuthorStorage()
	m := mutations.NewMutationServer(storage)
	q := queries.NewQueryAuthor(storage)

	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootQuery",
			Fields: q.GetRootFields(),
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name:   "RootMutation",
			Fields: m.GetRootFields(),
		}),
	}

	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		log.Fatalf("Failed to create new scheme, error: %v", err)
	}

	httpHandler := handler.New(&handler.Config{
		Schema:   &schema,
		GraphiQL: true,
	})

	e := echo.New()
	e.POST("/graphql", echo.WrapHandler(httpHandler))
	e.GET("/graphql", echo.WrapHandler(httpHandler))
	e.Logger.Fatal(e.Start(":8383"))

}
